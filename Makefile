# pir-regnskap.git/Makefile
# File ID: a2c261f6-0135-11e7-93a2-f74d993421b0

.PHONY: all
all:
	cd data && $(MAKE)

.PHONY: clean
clean:
	cd data && $(MAKE) clean

.PHONY: test
test:
	cd data && $(MAKE) test
