Dette er regnskapsfiler for Piratpartiet Norge i en så detaljert grad 
som mulig uten at personvernet brytes.

Det lagres i Ledger-format, et opensource regnskapsprogram som er 
tilgjengelig fra &lt;<http://ledger-cli.org>&gt;. Alle datafilene ligger 
i [data/](data/) og skrives i ren tekst med en standard teksteditor. 
Rapporter og andre dataformater genereres med ledger eller annen 
programvare fra disse filene.

Noen husregler:

- Alle filer skal være i UTF-8 med standard Unix (\n) linjeskift. Ingen 
  BOM (Byte Order Mark) skal noensinne brukes.
- Alle Git-commiter skal kun gjøre én ting. Dette er for å lettere 
  oppdage feil, og det er lett å revertere feil i etterkant uten å 
  fjerne urelatert funksjonalitet. For eksempel, whitespace-forandringer 
  legges ikke inn sammen med retting av skrivefeil eller innlegging av 
  nye kontotransaksjoner. Legg dem inn i tre commiter, en med kun 
  whitespace-forandringene, en som kun inneholder retting av skrivefeil, 
  og en som kun legger inn nye transaksjoner.
- Alle commiter som ender opp på master skal være gjort under committers 
  juridiske navn med en gyldig emailadresse.
- Bruk gode loggmeldinger i commiten, minst en linje i begynnelsen på 
  maksimalt 72 tegn. Hvis det er nødvendig å skrive mer, lag en tom 
  linje og legg inn teksten etter den. Maksimum linjelengde her er også 
  72 tegn.
- Historien på master er uforanderlig, og skal aldri rebases.
- Git-historien må være så ryddig som mulig. Arbeid bør foregå på andre 
  brancher der historien kan videreforedles før de merges til master.
- Alle commiter er atomiske på master, ingen halvferdige commiter med 
  kjente feil skal dit. Commiter som bryter funksjonaliteten (spesielt 
  "make", "make test" og "make clean") på master må squashes sammen til 
  én commit som ikke ødelegger.

----

    File ID: 55d30b98-013f-11e7-811d-f74d993421b0
    vim: set ts=2 sw=2 sts=2 tw=72 et fo=tcqw fenc=utf8 :
    vim: set com=b\:#,fb\:-,fb\:*,n\:> ft=markdown :
